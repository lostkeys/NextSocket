﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NextNet;
using Common;

namespace DemoClient
{
    public class Program
    {
        static NextClient client = new NextClient();

        static void Main(string[] args)
        {
            client.Start("192.168.3.63", 666);
            client.DisoposeHandle += Client_DisoposeHandle;
        }

        private static void Client_DisoposeHandle(object sender, DataDisp e)
        {
            if (e.Data == DataType.Base)
            {
                if (e.Type == MessType.ClientID)
                {
                    client.ClientID = e.ID;
                    client.FileSend(client.ClientID, @"D:\Temp\1.jpg", @"D:\");
                }

                
            }
            else if (e.Data == DataType.File)
            {
                client.FileDispose(e.Stream, e.ID, e.Type);
            }
        }
    }
}
