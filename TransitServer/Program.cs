﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NextNet;
using Common;

namespace TransitServer
{
    public class Program
    {
        static NextServer server = new NextServer();
        static HandleFile file = new HandleFile();

        static void Main(string[] args)
        {
            if(server.Start("192.168.3.63",666))
            {
                server.DisoposeHandle += Server_DisoposeHandle;
                Console.WriteLine("服务启动成功");
            }
            else
            {
                Console.WriteLine("服务启动失败");
            }
        }

        private static void Server_DisoposeHandle(object sender, DataDisp e)
        {
            if(e.Data==DataType.File)
            {
                file.FileDispose(e.Stream, e.ID, e.Type);
            }
            
            
        }
    }
}
