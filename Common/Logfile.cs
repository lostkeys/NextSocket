﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Common
{
    /// <summary>
    /// 日志类
    /// </summary>
    public class Logfile
    {
        private string ToFileNmae;//日志存储路径

        public Logfile(int type)
        {
            string path = (type == 1) ? "Server" : "Client";
            this.ToFileNmae = AppDomain.CurrentDomain.SetupInformation.ApplicationBase+ "Log\\" + path+"\\";
        }

        /// <summary>
        /// 创建日志
        /// </summary>
        /// <param name="message">日志信息</param>
        /// <param name="title">日志标题</param>
        public void Write(string message, string title)
        {
            string path = this.ToFileNmae;
            string filename = path + DateTime.Now.ToString("D") + ".txt";
            string cont = "";
            if (Directory.Exists(path) == false)
                Directory.CreateDirectory(path);
            FileInfo fileInf = new FileInfo(filename);
            if (File.Exists(filename))//如何文件存在 则在文件后面累加
            {
                FileStream myFss = new FileStream(filename, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);
                StreamReader r = new StreamReader(myFss);
                cont = r.ReadToEnd();
                r.Close();
                myFss.Close();
            }
            #region 生成文件日志
            FileStream myFs = new FileStream(filename, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite);
            StreamWriter n = new StreamWriter(myFs);
            n.WriteLine(cont);
            n.WriteLine("-------------------------------Begin-------------------------------------------");
            n.WriteLine("*****" + title + "*****");
            n.WriteLine("时间：" + DateTime.Now.ToString());
            n.WriteLine("信息：" + message);
            n.WriteLine("-------------------------------end---------------------------------------------");
            n.Close();
            myFs.Close();
            //
            if (fileInf.Length >= 1024 * 1024 * 200)
            {
                string NewName = path + "Log" + GetTime() + ".txt";
                File.Move(filename, NewName);
            }
            #endregion
        }
        /// <summary>
        /// 系统时间
        /// </summary>
        /// <returns></returns>
        string GetTime()
        {
            string dNow = DateTime.Now.ToString().Trim().Replace("/", "").Replace(":", "");
            string fileName = dNow.ToString();
            return fileName;
        }
    }
}
